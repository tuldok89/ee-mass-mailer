﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace EE_Mass_Mailer
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
            OkButton.Click += new EventHandler(OkButton_Click);
            this.Load += new EventHandler(Settings_Load);
        }

        void Settings_Load(object sender, EventArgs e)
        {
            UsernameTextBox.Text = Properties.Settings.Default.username;
            PasswordTextbox.Text = Properties.Settings.Default.password;
        }

        void OkButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.username = UsernameTextBox.Text;
            Properties.Settings.Default.password = PasswordTextbox.Text;
            Properties.Settings.Default.Save();
            this.Close();
        }
    }
}
