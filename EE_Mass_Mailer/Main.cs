﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace EE_Mass_Mailer
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
            MembersListButton.Click += new EventHandler(MembersListButton_Click);
            SettingsButton.Click += new EventHandler(SettingsButton_Click);
            SendMailButton.Click += new EventHandler(SendMailButton_Click);
        }

        void SendMailButton_Click(object sender, EventArgs e)
        {
            var form = new Sender();
            form.ShowDialog();
        }

        void SettingsButton_Click(object sender, EventArgs e)
        {
            var form = new Settings();
            form.ShowDialog();
        }

        void MembersListButton_Click(object sender, EventArgs e)
        {
            var form = new Memberslist();
            form.ShowDialog();
        }


    }
}
