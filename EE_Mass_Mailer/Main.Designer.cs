﻿namespace EE_Mass_Mailer
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SendMailButton = new System.Windows.Forms.Button();
            this.SettingsButton = new System.Windows.Forms.Button();
            this.MembersListButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SendMailButton
            // 
            this.SendMailButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SendMailButton.Location = new System.Drawing.Point(28, 45);
            this.SendMailButton.Name = "SendMailButton";
            this.SendMailButton.Size = new System.Drawing.Size(206, 48);
            this.SendMailButton.TabIndex = 0;
            this.SendMailButton.Text = "Send Mail";
            this.SendMailButton.UseVisualStyleBackColor = true;
            // 
            // SettingsButton
            // 
            this.SettingsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SettingsButton.Location = new System.Drawing.Point(28, 153);
            this.SettingsButton.Name = "SettingsButton";
            this.SettingsButton.Size = new System.Drawing.Size(206, 48);
            this.SettingsButton.TabIndex = 0;
            this.SettingsButton.Text = "Settings";
            this.SettingsButton.UseVisualStyleBackColor = true;
            // 
            // MembersListButton
            // 
            this.MembersListButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MembersListButton.Location = new System.Drawing.Point(28, 99);
            this.MembersListButton.Name = "MembersListButton";
            this.MembersListButton.Size = new System.Drawing.Size(206, 48);
            this.MembersListButton.TabIndex = 0;
            this.MembersListButton.Text = "Members List";
            this.MembersListButton.UseVisualStyleBackColor = true;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(262, 247);
            this.Controls.Add(this.MembersListButton);
            this.Controls.Add(this.SettingsButton);
            this.Controls.Add(this.SendMailButton);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Empire Mass Mailer";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button SendMailButton;
        private System.Windows.Forms.Button SettingsButton;
        private System.Windows.Forms.Button MembersListButton;
    }
}