﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Diagnostics;
using System.Web;

namespace EE_Mass_Mailer
{
    public partial class Memberslist : Form
    {
        public Memberslist()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(Properties.Resources.EE_Members);
            XmlNodeList members = xmlDoc.GetElementsByTagName("row");

            foreach (XmlNode member in members)
            {
                string[] memberstr = { HttpUtility.HtmlDecode(member["name"].InnerText),
                                         member["email"].InnerText,
                                         HttpUtility.UrlDecode(member["members_seo_name"].InnerText) };
                listView1.Items.Add(new ListViewItem(memberstr));
            }
        }
    }
}
